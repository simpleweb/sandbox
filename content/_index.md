+++
title = "Bac à sable"
description = "Tester comment fonctionne l'éditeur du projet «simple web»!"
+++

Vous pouvez éditer ce bac à sable en allant sur <a href="http://edit.sandbox.acoeuro.com">http://edit.sandbox.acoeuro.com</a>

Le code source du projet, avec la <em>plate forme</em>, l'<em>éditeur</em> et le <em>bac à sable</em>, sont tous dans le projet framagit nommé [simpleWeb](https://framagit.org/simpleweb).

Ici tout code <a href="https://daringfireball.net/projects/markdown/syntax">markdown</a> devrait fonctionner, mais aussi du html brut. Avec cet éditeur graphique (tinymce) vous pouvez même faire un copier-coller depuis une autre page web.

<strong>Attention:</strong> ce bac à sable est <span style="color: #ff6600;">remis à zéro</span> régulièrement...
